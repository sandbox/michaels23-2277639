// $Id: README.txt,v 1.1 2014/05/31 22:27:17 michaels23 Exp $

Readme file for the VIEWS CAPTCHA module for Drupal
---------------------------------------------

views_captcha.module provides the ability to require a CAPTCHA 
before viewing specific view pages.

Installation:
  Installation is like with all normal drupal modules:
  extract the 'views_captcha' folder from the tar ball to the
  modules directory from your website (typically sites/all/modules).

Dependencies:
  The Views CAPTCHA module requires the CATPCHA and Views modules.

Conflicts/known issues:

Configuration:
  The configuration page is at admin/settings/views_captcha. 
  There you can select which views to protect,
  and which CAPTCHA to protect them with.